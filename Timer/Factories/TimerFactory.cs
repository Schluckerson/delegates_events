﻿using System;

namespace Timer.Factories
{
	public class TimerFactory
	{
		public Timer CreateTimer(string name, int ticks)
		{
			Timer timer = new Timer(name, ticks);
			return timer;
		}
	}
}