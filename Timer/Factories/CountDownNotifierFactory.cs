﻿using System;
using Timer.Interfaces;
using Timer.Implementation;

namespace Timer.Factories
{
	public class CountDownNotifierFactory
	{
		public ICountDownNotifier CreateNotifierForTimer(Timer timer)
		{
			CountDownNotifier countDownNotifier = new CountDownNotifier(timer);
			return countDownNotifier;
		}
	}
}
