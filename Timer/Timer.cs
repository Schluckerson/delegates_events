﻿using System;
using System.Threading;

namespace Timer
{
	public class Timer
	{
		readonly string name;
		int tick;
		public event Action<string,int> Started;
		public event Action<string,int> Tick;
		public event Action<string> Stopped;
		public Timer(string name, int tick)
        {
			if (name == "" || name == null || tick <= 0) throw new ArgumentException("Arguments are incorrect");
			this.name = name;
			this.tick = tick;	
			
        }

		public void Start()
        {			
			Started?.Invoke(name, tick);
			while(tick > 1)
            {
				tick -= 1;
				Tick?.Invoke(name, tick);
				Thread.Sleep(100);
            }
			Stopped?.Invoke(name);
		}
	}
}