﻿using System;
using Timer.Interfaces;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		private readonly Timer _timer;

		public CountDownNotifier(Timer timer)
        {
			if (timer == null) throw new ArgumentNullException("timer", "Timer is null");
			_timer = timer;
        }
		public void Init(Action<string, int> startDelegate, Action<string> stopDelegate, Action<string, int> tickDelegate)
		{
				_timer.Started += startDelegate;
				_timer.Stopped += stopDelegate;
				_timer.Tick += tickDelegate;
		}

		public void Run()
		{
			_timer.Start();
		}
	}
}